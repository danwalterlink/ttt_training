interface IAdaptation =
  { referenceLift: Lift
  , sessions: SessionMap
  , progression: Progression
  , referenceMap: referenceMap
  , idealReferenceMap: idealRefernceMap
  }

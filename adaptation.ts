// concept of adaptation:
// global adaptation with regard to shift in movement

// need to model:
// fatigue
// globalAdaptation
// 
// defined on progression on exercise:
const referenceLift;
const adaptation = 
  ({ reference
  , truePercentageMap
  , idealPercentageMap
  , progression
  , regimen
  }) => {
  
  }
const load = 
  ({ regimen
  , truePercentageMap
  , rest
  }) => {
  
  }

const idealPercentageMap =
  { squat:
    { frontSquat: .85
    , cleanDeadlift: 1
    , snatchDeadlift: .9
    , powerliftDeadlift: 1.2
    , benchpress: .75
    , powerliftingDeadlift: 1.2
    , militaryPress: .45
    , inclinebenchPress: .6
    , pushPress: .6375
    , barbellrow: .525
    }
  , benchpress:
    { closegripBenchpress: .9
    , pushpress: .85
    , inclinepress: .8
    , militarypress: .6
    , dip: 1.05
    , supinatedChinup: .9
    , barbellRow: .7
    , preachercurl: .4
    , standingreversecurl: .35
    }
  }
